#pragma once
#include "Json\json.h"
#include <ios>
#include <iostream>

namespace Tools{
	Json::Value string_to_json(std::string json_data);
	std::string json_to_string(Json::Value json_string);
}