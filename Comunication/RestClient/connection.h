#ifndef INCLUDE_RESTCLIENT_CPP_CONNECTION_H_
#define INCLUDE_RESTCLIENT_CPP_CONNECTION_H_

#include <curl/curl.h>
#include <string>
#include <map>
#include <cstdlib>

#include "restclient.h"

namespace RestClient {
	class Connection {
	public:
		typedef struct {
			double totalTime;
			double nameLookupTime;
			double connectTime;
			double appConnectTime;
			double preTransferTime;
			double startTransferTime;
			double redirectTime;
			int redirectCount;
		} RequestInfo;
		typedef struct {
			std::string baseUrl;
			RestClient::HeaderFields headers;
			int timeout;
			bool followRedirects;
			bool noSignal;
			struct {
				std::string username;
				std::string password;
			} basicAuth;

			std::string certPath;
			std::string certType;
			std::string keyPath;
			std::string keyPassword;
			std::string customUserAgent;
			std::string uriProxy;
			RequestInfo lastRequest;
		} Info;
		
		explicit Connection(const std::string& baseUrl);
		~Connection();

		// Instance configuration methods
		// configure basic auth
		void SetBasicAuth(const std::string& username,const std::string& password);

		// set connection timeout to seconds
		void SetTimeout(int seconds);

		// set to not use signals
		void SetNoSignal(bool no);

		// set whether to follow redirects
		void FollowRedirects(bool follow);

		// set custom user agent
		// (this will result in the UA "foo/cool restclient-cpp/VERSION")
		void SetUserAgent(const std::string& userAgent);

		// set the Certificate Authority (CA) Info which is the path to file holding
		// certificates to be used to verify peers. See CURLOPT_CAINFO
		void SetCAInfoFilePath(const std::string& caInfoFilePath);

		// set CURLOPT_SSLCERT
		void SetCertPath(const std::string& cert);

		// set CURLOPT_SSLCERTTYPE
		void SetCertType(const std::string& type);

		// set CURLOPT_SSLKEY. Default format is PEM
		void SetKeyPath(const std::string& keyPath);

		// set CURLOPT_KEYPASSWD.
		void SetKeyPassword(const std::string& keyPassword);

		// set CURLOPT_PROXY
		void SetProxy(const std::string& uriProxy);

		std::string GetUserAgent();

		RestClient::Connection::Info GetInfo();

		// set headers
		void SetHeaders(RestClient::HeaderFields headers);

		// get headers
		RestClient::HeaderFields GetHeaders();

		// append additional headers
		void AppendHeader(const std::string& key,const std::string& value);
		
		// Basic HTTP verb methods
		RestClient::Response get(const std::string& uri);
		RestClient::Response post(const std::string& uri,const std::string& data);
		RestClient::Response put(const std::string& uri,const std::string& data);
		RestClient::Response del(const std::string& uri);
		RestClient::Response head(const std::string& uri);

	private:
		CURL* curlHandle;
		std::string baseUrl;
		RestClient::HeaderFields headerFields;
		int timeout;
		bool followRedirects;
		bool noSignal;
		struct {
			std::string username;
			std::string password;
		} basicAuth;
		std::string customUserAgent;
		std::string caInfoFilePath;
		RequestInfo lastRequest;
		std::string certPath;
		std::string certType;
		std::string keyPath;
		std::string keyPassword;
		std::string uriProxy;
		RestClient::Response performCurlRequest(const std::string& uri);
	};
};

#endif 
