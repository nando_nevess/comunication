#pragma once
#include "Utility.h"

namespace Tools{
	Json::Value string_to_json(std::string json_data){
		Json::Value root;
		Json::Reader reader;
		bool parsingSuccessful = reader.parse(json_data.c_str(), root);
		if (!parsingSuccessful)
			return Json::Value();

		return root;
	}
	std::string json_to_string(Json::Value json_string){
		return json_string.toStyledString();
	}
}