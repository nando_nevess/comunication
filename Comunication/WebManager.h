#pragma once
#include <iostream>
#include <stdint.h>

#include "RestClient\restclient.h"
#include "Utility.h"

#include <Wininet.h>
#pragma comment(lib, "Wininet.lib")

class WebManager{
public:

	std::string base_url = "http://api.scutumnet.com";

	std::string get_motd(uint32_t current_action_id){
		std::string url_end = base_url + "/motd";
		url_end += "/" + std::to_string(current_action_id);

		RestClient::Response r = RestClient::get(url_end);
		std::string response_body = r.body;

		return response_body;
	}

	uint32_t get_last_motd_id(){
		std::string url_end = base_url + "/motd";

		RestClient::Response r = RestClient::get(url_end);
		std::string response_body = r.body;

		Json::Value& response_json = Tools::string_to_json(response_body);
		if (response_json.isNull() || response_json.empty())
			return 0;

		uint32_t retval = 0;
		if (!response_json["id"].isNull() && !response_json["id"].empty())
			retval = response_json["id"].asInt();

		return retval;
	}

	uint32_t start_game(uint32_t software_id,std::string user_email, std::string user_token, uint32_t login_id, std::string game_name){
		std::string url_end = base_url + "/start_game";

		std::map<std::string, std::string> params;
		
		params["user_email"] = user_email;
		params["user_token"] = user_token;
		params["game_name"] = game_name;
		params["software_id"] = std::to_string(software_id);
		params["login_id"] = std::to_string(login_id);

		RestClient::Response r = RestClient::post(url_end, "application/x-www-form-urlencoded", params);
		std::string response_body = r.body;

		int id;
		try{
			id = std::stoi(response_body);
		}
		catch (const std::exception&){
			id = 0;
		}

		return id;
	}
	uint32_t start_login(std::string user_email,std::string user_token, uint32_t software_id,uint32_t software_version,std::string machine_id) {
		std::string url_end = base_url + "/login";

		std::map<std::string, std::string> params;

		params["user_email"] = user_email;
		params["user_token"] = user_token;
		params["machine_id"] = machine_id;
		params["software_id"] = std::to_string(software_id);
		params["software_version"] = std::to_string(software_version);

		RestClient::Response r = RestClient::post(url_end, "application/x-www-form-urlencoded", params);
		std::string response_body = r.body;

		int id;
		try {
			id = std::stoi(response_body);
		}
		catch (const std::exception&) {
			id = 0;
		}

		return id;
	}
	
	bool update_game(uint32_t software_id, std::string user_email, std::string user_token, uint32_t connection_id){
		std::string url_end = base_url + "/update_game";

		std::map<std::string, std::string> params;

		params["user_email"] = user_email;
		params["user_token"] = user_token;
		params["software_id"] = std::to_string(software_id);
		params["game_connection_id"] = std::to_string(connection_id);

		RestClient::Response r = RestClient::post(url_end, "application/x-www-form-urlencoded", params);
		std::string response_body = r.body;

		return true;
	}
	bool update_login(std::string user_email, std::string user_token ,uint32_t connection_id, uint32_t software_id) {
		std::string url_end = base_url + "/update_login";

		std::map<std::string, std::string> params;

		params["user_email"] = user_email;
		params["user_token"] = user_token;
		params["login_id"] = std::to_string(connection_id);
		params["software_id"] = std::to_string(software_id);
		
		RestClient::Response r = RestClient::post(url_end, "application/x-www-form-urlencoded", params);
		std::string response_body = r.body;

		if (response_body == "true")
			return true;

		return false;
	}

	bool check_mismatch_machine_id(std::string machine_id, std::string user_email){
		/*std::string url_end = base_url + "/mismatch_machine_id";

		RestClient::Response r = RestClient::post(url_end, "application/x-www-form-urlencoded");
		//RestClient::Response r = RestClient::post(url_end, "text/json", "{\"foo\": \"bla\"}");
		std::string response_body = r.body;

		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY START - - - - - - - - - - - - - - - - - - - - \n\n" << response_body;
		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY END- - - - - - - - - - - - - - - - - - - - - -";
		
		//TE AMO MINHA MINI MONSTRO

		//COLOCAR ALGO PARA VERIFICAR A RESPOSTA*/
		return true;
	}

	bool check_connection(){
		if (InternetCheckConnection("http://www.google.com", FLAG_ICC_FORCE_CONNECTION, 0))
			return true;
		
		std::cout << "\nConnection is offline;";
		return false;
	}

	static WebManager* get(){
		static WebManager* mWebManager = nullptr;
		if (!mWebManager)
			mWebManager = new WebManager();
		return mWebManager;
	}
};