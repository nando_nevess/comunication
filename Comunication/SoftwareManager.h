#pragma once
#include "Json\json.h"
#include "WebManager.h"
#include "Utility.h"
#include <iostream>
#include <ios>
#include <Windows.h>
#include <memory>
#include "Comunication.h"
#include "Time.h"

struct GameInfo {
	std::string process_name = "";
	TimeChronometer timer;
};

struct MessageInfo{
	std::string message = "";
	uint32_t size = 0;
	uint32_t interval_time = 0;
	uint32_t number_repeats = 0;
};

class SoftwareManager{
	SoftwareManager(){
		current_message = std::make_shared<MessageInfo>();
	}
	~SoftwareManager(){}

	std::map<uint32_t /*CONNECTION_ID*/, std::shared_ptr<GameInfo>> connections_list;
	uint32_t connection_login_id = 0;

public:
	void set_connection_login_id(uint32_t connection_id) {
		this->connection_login_id = connection_id;
	}
	uint32_t get_connection_login_id() {
		return this->connection_login_id;
	}

	Json::Value string_to_json(std::string json_data){
		Json::Value root;
		Json::Reader reader;
		bool parsingSuccessful = reader.parse(json_data.c_str(), root);
		if (!parsingSuccessful){
			std::cout << "\nFailed to parse" << reader.getFormattedErrorMessages();
			return 0;
		}

		return root;
	}
	std::string json_to_string(Json::Value json_string){
		return json_string.toStyledString();
	}
	
	wchar_t* string_to_wchar(std::string path_string){
		std::wstring wstring(path_string.begin(), path_string.end());
		return const_cast<wchar_t*>(wstring.c_str());
	}
	std::string wchar_to_string(wchar_t* temp){
		std::wstring tes(temp);
		return std::string(tes.begin(), tes.end());
	}

	std::shared_ptr<MessageInfo> current_message;// = std::make_shared<MessageInfo>();

	void thread_refresh_motd(){
		std::cout << "\n\n\n";

		uint32_t current_motd_id = WebManager::get()->get_last_motd_id();
		current_motd_id++;

		while (true){
			Sleep(5 * 1000);
			std::cout << "\nStart Loop Current_motd_id: " << current_motd_id;
			
			if (!WebManager::get()->check_connection())
				continue;

			std::string motd_json_string = WebManager::get()->get_motd(current_motd_id);
			Json::Value& motd_json = Tools::string_to_json(motd_json_string);
			if (motd_json.isNull() || motd_json.empty())
				continue;

			std::cout << "\n\n JSON" << motd_json.toStyledString();
								
			std::string message_text = "";
			if (!motd_json["message"].isNull() && !motd_json["message"].empty())
				message_text = motd_json["message"].asString();

			uint32_t leng = 0;
			if (!motd_json["message_size"].isNull() && !motd_json["message_size"].empty())
				leng = motd_json["message_size"].asInt();

			uint32_t interval_time = 0;
			if (!motd_json["interval_time"].isNull() && !motd_json["interval_time"].empty())
				interval_time = motd_json["interval_time"].asInt();
			
			uint32_t number_repeats = 0;
			if (!motd_json["number_repeats"].isNull() && !motd_json["number_repeats"].empty())
				number_repeats = motd_json["number_repeats"].asInt();
						
			if (interval_time > 0){
				//COLOCAR MUTEX
				if (current_message) {
					current_message->message = message_text;
					current_message->size = leng;
					current_message->interval_time = interval_time;
					current_message->number_repeats = number_repeats;
				}
				//TIRAR MUTEX

				current_motd_id++;
				continue;
			}

			std::wstring wstring(message_text.begin(), message_text.end());
			wchar_t* message_wchar = (wchar_t*)wstring.c_str();

			if (!Comunication::get()->execute_message_callback(message_wchar, leng))
				continue;

			current_motd_id++;
		}
	}
	void thread_refresh_motd_times(){
		uint32_t loop_count = 0;

		while (true){
			Sleep(20 * 1001);

			if (!WebManager::get()->check_connection())
				continue;

			if (!current_message)
				continue;

			if (loop_count >= current_message->number_repeats)
				continue;			

			Sleep(current_message->interval_time * 1000);

			std::wstring wstring(current_message->message.begin(), current_message->message.end());
			wchar_t* message_wchar = (wchar_t*)wstring.c_str();

			Comunication::get()->execute_message_times_in_times_callback(message_wchar, current_message->size);
			loop_count++;
		}

	}
	void thread_update_info() {
		std::cout << "\n";
		while(true){
			Sleep(10 * 1000);

			if (!WebManager::get()->check_connection())
				continue;

			std::cout << "\n";

			std::cout << "\n CONNECTION LOGIN ID: " << this->connection_login_id;
			if(this->connection_login_id != 0)
				Comunication::get()->update_login(this->connection_login_id);
			
			if (!connections_list.size())
				continue;

			for (auto current_gameInfo : connections_list) {
				if (!current_gameInfo.second)
					continue;

				std::cout << "\n UPDATE GAME: " << current_gameInfo.second->process_name << " - CONNECTION ID: " << current_gameInfo.first;

				std::wstring wstring(current_gameInfo.second->process_name.begin(), current_gameInfo.second->process_name.end());
				
				bool result_update = false;
				bool result = Comunication::get()->execute_update_game_callback((wchar_t*)wstring.c_str(), wstring.size());
				if(result)
					result_update = Comunication::get()->update_game(current_gameInfo.first);

				std::cout << "\n IS OPEN GAME: " << result << " - RESULT UPDATE: " << result_update;
			}
		}
	}

	std::pair<uint32_t, std::shared_ptr<GameInfo>> get_connection_game(std::string process_name) {
		for (auto current_gameInfo : connections_list) {
			if (current_gameInfo.second->process_name == process_name)
				return std::make_pair(current_gameInfo.first,current_gameInfo.second);
		}
		return std::make_pair(0, nullptr);
	}

	void add_connections_list(uint32_t connection_id, std::shared_ptr<GameInfo> gameInfo) {
		std::shared_ptr<GameInfo> current_gameInfo = connections_list[connection_id];
		if (!current_gameInfo)
			connections_list[connection_id] = gameInfo;

		std::map<uint32_t, std::shared_ptr<GameInfo>>::iterator itr = connections_list.begin();
		while (itr != connections_list.end()) {
			if (itr->second->timer.elapsed_milliseconds() > (10 * 60 * 1000))
				itr = connections_list.erase(itr);
			else
				itr++;
		}
	}
	
	void init_threads();

	static SoftwareManager* get(){
		static SoftwareManager* mSoftwareManager = nullptr;
		if (!mSoftwareManager)
			mSoftwareManager = new SoftwareManager();
		return mSoftwareManager;
	}
};