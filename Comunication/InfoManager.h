#pragma once
#include "Comunication.h"


class InfoManager{
	std::string current_user_email = "";
	uint32_t current_execute_id = 0;
	uint32_t current_software_id = 0;
	uint32_t current_software_version = 0;
	uint32_t current_connection_game_id = 0;
	bool current_user_trial = false;
	std::pair<void*, uint32_t> current_user_token = std::make_pair(nullptr,0);
	std::pair<void*, uint32_t> current_machine_id = std::make_pair(nullptr, 0);
	
public:
	bool get_current_user_trial() { return this->current_user_trial; }
	uint32_t get_current_execute_id(){ return this->current_execute_id; }
	uint32_t get_current_software_version(){ return this->current_software_version; }
	uint32_t get_current_software_id(){ return this->current_software_id; }
	uint32_t get_current_connection_game_id(){ return this->current_connection_game_id; }
	std::string get_current_user_email() { return this->current_user_email; }
	std::pair<void*, uint32_t> get_current_user_token(){ return this->current_user_token; }
	std::pair<void*, uint32_t> get_current_machine_id(){ return this->current_machine_id; }

	void set_current_user_trial(bool user_trial) { this->current_user_trial = user_trial; }
	void set_current_user_email(std::string user_email){ this->current_user_email = user_email; }
	void set_current_connection_game_id(uint32_t in){ this->current_connection_game_id = in; }
	void set_current_execute_id(uint32_t execute_id){ this->current_execute_id = execute_id; }
	void set_current_software_version(uint32_t software_version){ this->current_software_version = software_version; }
	void set_current_software_id(uint32_t software_id){ this->current_software_id = software_id; }
	void set_current_user_token(void* user_token, uint32_t len){ this->current_user_token = std::make_pair(user_token, len); }
	void set_current_machine_id(void* machine_id, uint32_t len){ this->current_machine_id = std::make_pair(machine_id, len); }

	static InfoManager* get(){
		static InfoManager* mInfoManager = nullptr;
		if (!mInfoManager)
			mInfoManager = new InfoManager();
		return mInfoManager;
	}
};