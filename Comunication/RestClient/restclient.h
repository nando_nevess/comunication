#ifndef INCLUDE_RESTCLIENT_CPP_RESTCLIENT_H_
#define INCLUDE_RESTCLIENT_CPP_RESTCLIENT_H_

#include <curl/curl.h>
#include <string>
#include <map>
#include <cstdlib>

#pragma comment(lib, "libcurl.lib")

namespace RestClient {
	typedef std::map<std::string, std::string> HeaderFields;

	typedef struct {
		int code;
		std::string body;
		HeaderFields headers;
	} Response;

	int init();
	void disable();

	Response get(const std::string& url);
	Response post(const std::string& url,const std::string& content_type,const std::map<std::string, std::string>& data = std::map<std::string, std::string>());
	Response put(const std::string& url,const std::string& content_type,const std::string& data);
	Response del(const std::string& url);
	Response head(const std::string& url);
}
#endif
