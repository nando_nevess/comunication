#pragma once
#include <ios>
#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include "stdafx.h"
#include "ComunicationDll.h"

#include "InfoManager.h"

#ifdef DLLDIR_EX
	#define DLLDIR  __declspec(dllexport)
#else
	#define DLLDIR  __declspec(dllimport)
#endif 

class Comunication{
	params_t::message_callback message_callback = nullptr;
	params_t::message_times_in_times_callback message_times_in_times_callback = nullptr;
	params_t::machine_mismatch_callback machine_mismatch_callback = nullptr;
	params_t::update_game_callback update_game_callback = nullptr;

public:
	bool execute_message_callback(wchar_t* message, uint32_t len){
		if (!message_callback)
			return false;

		message_callback(message, len);
		return true;
	}
	bool execute_machine_callback(){
		if (!machine_mismatch_callback)
			return false;

		machine_mismatch_callback();
		return true;
	}
	bool execute_message_times_in_times_callback(wchar_t* message, uint32_t len){
		if (!message_times_in_times_callback)
			return false;

		message_times_in_times_callback(message, len);
		return true;
	}
	bool execute_update_game_callback(wchar_t* message, uint32_t len) {
		if (!update_game_callback)
			return false;

		return this->update_game_callback(message, len);
	}

	int set_software_version(uint32_t software_version){ 
		InfoManager::get()->set_current_software_version(software_version);
		return 0;
	}
	int set_software_id(uint32_t software_id){ 
		InfoManager::get()->set_current_software_id(software_id);
		return 0;
	}

	int set_user_email(wchar_t* user_email){ 
		std::wstring tes(user_email);
		std::string temp(tes.begin(), tes.end());
		InfoManager::get()->set_current_user_email(temp);
		return 0;
	}
	int set_user_token(void* data, uint32_t len){ 
		InfoManager::get()->set_current_user_token(data, len);
		return 0;
	}
	int set_machine_id(void* data, uint32_t len){ 
		InfoManager::get()->set_current_machine_id(data, len);
		return 0;
	}
	int set_user_trial(bool trial) {
		InfoManager::get()->set_current_user_trial(trial);
		return 0;
	}

	int set_message_times_in_times_callback(params_t::message_callback _callback){
		if (!_callback)
			return 0;
		
		message_times_in_times_callback = _callback;
		return 0;
	}
	int set_message_callback(params_t::message_callback _callback){
		if (!_callback)
			return 0;
		
		message_callback = _callback;
		return 0;
	}
	int set_machine_mismatch_callback(params_t::machine_mismatch_callback _callback){
		if (!_callback)
			return 0;

		machine_mismatch_callback = _callback;
		return 0;
	}
	int set_update_game_callback(params_t::update_game_callback _callback) {
		if (!_callback)
			return 0;

		update_game_callback = _callback;
		return 0;
	}
		
	bool check_mismatch_machine_id();

	int start_login();
	bool update_login(uint32_t connection_id);

	int start_game(wchar_t* game_name);
	bool update_game(uint32_t connection_id);

	int run();
	
	static Comunication* get() {
		static Comunication* mComunication = nullptr;
		if (!mComunication)
			mComunication = new Comunication;

		return mComunication;
	}
};
