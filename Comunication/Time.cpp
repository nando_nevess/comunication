#include "Time.h"

TimeChronometer::TimeChronometer(){
	reset();
}

void TimeChronometer::reset(){
	start_time = std::chrono::high_resolution_clock::now();
}

int32_t TimeChronometer::elapsed_milliseconds(){
	return (int32_t)(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start_time)).count();
}

int32_t TimeChronometer::elapsed_microseconds(){
	return (int32_t)(std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start_time)).count();
}

int32_t TimeChronometer::elapsed_nanoseconds(){
	return (int32_t)(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - start_time)).count();
}

int32_t TimeChronometer::elapsed_seconds(){
	return (int32_t)(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - start_time)).count();
}

int Time::get_current_time(int type){
	_SYSTEMTIME Time;
	GetSystemTime(&Time);
	switch (type){
	case 1:
		return Time.wYear;
		break;
	case 2:
		return Time.wMonth;
		break;
	case 3:
		return Time.wDayOfWeek;
		break;
	case 4:
		return Time.wDay;
		break;
	case 5:
		return Time.wHour;
		break;
	case 6:
		return Time.wMinute;
		break;
	case 7:
		return Time.wSecond;
		break;
	case 8:
		return Time.wMilliseconds;
		break;
	}
	return 0;
}


bool TimeChronometer::wait_condition(std::function<bool()> condition, uint32_t timeout, uint32_t callback_check_delay){
	TimeChronometer timer;
	while ((uint32_t)timer.elapsed_milliseconds() < timeout){
		if (condition())
			return true;
		Sleep(callback_check_delay);
	}
	return false;
}

void TimeChronometer::wait_complete_delay(uint32_t timeout){
	while ((uint32_t)elapsed_milliseconds() < timeout){
		Sleep(1);
	}
}

TimeChronometer* TimeChronometer::get(){
	static TimeChronometer* mTimeChronometer = nullptr;
	if (!mTimeChronometer)
		mTimeChronometer = new TimeChronometer;
	return mTimeChronometer;
}