#pragma once
#include "SoftwareManager.h"
#include "WebManager.h"
#include <thread>

void SoftwareManager::init_threads(){
	std::thread([&](){ thread_refresh_motd_times(); }).detach();
	std::thread([&](){ thread_update_info(); }).detach();
}
