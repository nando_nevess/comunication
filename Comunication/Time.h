#pragma once
/*
WORD wYear;
WORD wMonth;
WORD wDayOfWeek;
WORD wDay;
WORD wHour;
WORD wMinute;
WORD wSecond;
WORD wMilliseconds;
*/

#include <windows.h>
#include <chrono>
#include <functional>

#define THREAD_TIMER_DECLARE\
	TimeChronometer _thread_timer;

#define THREAD_TIMER_RESET\
	_thread_timer.reset();


#define THREAD_LEFT_TIME_TO_WAIT(TOTAL) TOTAL - _thread_timer.elapsed_milliseconds();

#define THREAD_TIMER_WAIT_TOTAL(TOTAL){\
	int32_t _timer_total_sum = THREAD_LEFT_TIME_TO_WAIT(TOTAL)\
	if(_timer_total_sum > 0)\
		Sleep(_timer_total_sum);\
				}

class Time{
public:
	static int get_current_time(int type);
};

class TimeChronometer{
	std::chrono::system_clock::time_point start_time;

public:
	TimeChronometer();
	void reset();
	int32_t elapsed_milliseconds();
	int32_t elapsed_microseconds();
	int32_t elapsed_nanoseconds();
	int32_t elapsed_seconds();
	void reset_time_with_timeout(int32_t temp = 2000){
		start_time = std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(5000);
	}
	static bool wait_condition(std::function<bool()> condition, uint32_t timeout = 1000, uint32_t callback_check_delay = 10);

	void wait_complete_delay(uint32_t timeout);

	static TimeChronometer* get();
};


class FunctionCallbackAuto{
	std::function<void()> callback;
public:
	FunctionCallbackAuto(std::function<void()> callback_ptr){
		callback = callback_ptr;
	}

	~FunctionCallbackAuto(){
		if (callback)
			callback();
	}
};


