#pragma once
#include <Windows.h>

namespace params_t {
	typedef bool(*update_game_callback)(wchar_t* message, uint32_t message_len);

	typedef void(*message_callback)(wchar_t* message, uint32_t message_len);
	typedef void(*message_times_in_times_callback)(wchar_t* message, uint32_t message_len);
	typedef void(*machine_mismatch_callback)();
}

namespace methods_t {
	typedef void(*set_update_game_callback)(params_t::update_game_callback _callback);

	typedef void(*set_message_callback)(params_t::message_callback _callback);
	typedef void(*set_machine_callback)(params_t::machine_mismatch_callback _callback);
	typedef void(*set_message_times_in_times_callback)(params_t::message_callback _callback);

	typedef void(*check_mismatch_machine_id)();

	typedef void(*set_user_email)(wchar_t* user_email);
	typedef void(*set_software_id)(uint32_t software_id);
	typedef void(*set_software_version)(uint32_t software_version);

	typedef void(*set_user_token)(void* data, uint32_t len);
	typedef void(*set_machine_id)(void* data, uint32_t len);
	typedef void(*set_user_trial)(bool trial);

	typedef void(*set_user)(wchar_t* str);

	typedef bool(*update_game)(uint32_t str);
	typedef int(*start_game)(wchar_t* str);

	typedef bool(*update_login)(uint32_t str);
	typedef int(*start_login)();

	typedef void(*run)();
}

class ComunicationDll{
	bool loaded = false;

	bool load(){
		HMODULE _module = LoadLibrary("Comunication.dll");
		if (!_module)
			return false;

		*(int*)&set_update_game_callback = (int)GetProcAddress(_module, "set_update_game_callback");
		if (!set_update_game_callback)
			return false;

		*(int*)&start_login = (int)GetProcAddress(_module, "start_login");
		if (!start_login)
			return false;

		*(int*)&update_login = (int)GetProcAddress(_module, "update_login");
		if (!update_login)
			return false;

		*(int*)&update_game = (int)GetProcAddress(_module, "update_game");
		if (!update_game)
			return false;

		*(int*)&start_game = (int)GetProcAddress(_module, "start_game");
		if (!start_game)
			return false;
		
		*(int*)&set_message_times_in_times_callback = (int)GetProcAddress(_module, "set_message_times_in_times_callback");
		if (!set_message_times_in_times_callback)
			return false;

		*(int*)&check_mismatch_machine_id = (int)GetProcAddress(_module, "check_mismatch_machine_id");
		if (!check_mismatch_machine_id)
			return false;

		*(int*)&set_software_id = (int)GetProcAddress(_module, "set_software_id");
		if (!set_software_id)
			return false;

		*(int*)&set_user_email = (int)GetProcAddress(_module, "set_user_email");
		if (!set_user_email)
			return false;

		*(int*)&set_message_callback = (int)GetProcAddress(_module, "set_message_callback");
		if (!set_message_callback)
			return false;

		*(int*)&set_machine_callback = (int)GetProcAddress(_module, "set_machine_callback");
		if (!set_machine_callback)
			return false;

		*(int*)&set_software_version = (int)GetProcAddress(_module, "set_software_version");
		if (!set_software_version)
			return false;

		*(int*)&set_user_token = (int)GetProcAddress(_module, "set_user_token");
		if (!set_user_token)
			return false;

		*(int*)&set_machine_id = (int)GetProcAddress(_module, "set_machine_id");
		if (!set_machine_id)
			return false;

		*(int*)&run = (int)GetProcAddress(_module, "run");
		if (!run)
			return false;
		
		return true;
	}

	ComunicationDll() {
		loaded = load();
	}
public:

	bool is_loaded(){
		return loaded;
	}

	methods_t::set_message_callback set_message_callback;
	methods_t::set_machine_callback set_machine_callback;
	methods_t::set_message_times_in_times_callback set_message_times_in_times_callback;
	methods_t::set_update_game_callback set_update_game_callback;
		methods_t::run run;

	methods_t::start_game start_game;
	methods_t::start_login start_login;

	methods_t::update_game update_game;
	methods_t::update_login update_login;

	methods_t::check_mismatch_machine_id check_mismatch_machine_id;

	methods_t::set_user_email set_user_email;
	methods_t::set_software_id set_software_id;
	methods_t::set_software_version set_software_version;

	methods_t::set_user_token set_user_token;
	methods_t::set_machine_id set_machine_id;
	methods_t::set_user set_user;

	static ComunicationDll* get() {
		static ComunicationDll* mComunication = nullptr;
		if (!mComunication)
			mComunication = new ComunicationDll;
		return mComunication;
	}
};