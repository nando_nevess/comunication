#pragma once
#include "Comunication.h"
#include "SoftwareManager.h"
#include "WebManager.h"
#include "stdafx.h"


int Comunication::run(){
	SoftwareManager::get()->init_threads();
	return 0;
}

int Comunication::start_login() {
	std::string user_email = InfoManager::get()->get_current_user_email();

	std::pair<void*, uint32_t> user_token_pair = InfoManager::get()->get_current_user_token();
	if (!user_token_pair.first)
		return 0;
	
	/*std::pair<void*, uint32_t> machine_id_pair = InfoManager::get()->get_current_machine_id();
	if (!machine_id_pair.first)
		return 0;*/

	std::string* user_token_ptr = (static_cast<std::string*>(user_token_pair.first));
	//std::string* machine_id_ptr = (static_cast<std::string*>(machine_id_pair.first));
	
	std::string user_token = std::string(user_token_ptr->begin(), user_token_ptr->end());
	std::string machine_id = "null";//std::string(machine_id_ptr->begin(), machine_id_ptr->end());

	uint32_t software_id = InfoManager::get()->get_current_software_id();
	uint32_t software_version = InfoManager::get()->get_current_software_version();

	uint32_t connection_login_id = WebManager::get()->start_login(user_email, user_token, software_id, software_version, machine_id);
	SoftwareManager::get()->set_connection_login_id(connection_login_id);

	return connection_login_id;
}
bool Comunication::update_login(uint32_t connection_id) {
	std::string user_email = InfoManager::get()->get_current_user_email();

	std::pair<void*, uint32_t> user_token_pair = InfoManager::get()->get_current_user_token();
	if (!user_token_pair.first)
		return 0;

	std::string* user_token_ptr = (static_cast<std::string*>(user_token_pair.first));
	std::string user_token = std::string(user_token_ptr->begin(), user_token_ptr->end());
	uint32_t software_id = InfoManager::get()->get_current_software_id();

	return WebManager::get()->update_login(user_email, user_token, connection_id, software_id);
}

int Comunication::start_game(wchar_t* game_name){
	std::string user_email = InfoManager::get()->get_current_user_email();
	std::string name_game = SoftwareManager::get()->wchar_to_string(game_name);

	std::pair<void*, uint32_t> user_token_pair = InfoManager::get()->get_current_user_token();
	if (!user_token_pair.first)
		return 0;
	
	std::string* user_token_ptr = (static_cast<std::string*>(user_token_pair.first));
	std::string user_token = std::string(user_token_ptr->begin(), user_token_ptr->end());
	uint32_t software_id = InfoManager::get()->get_current_software_id();
	uint32_t login_id = SoftwareManager::get()->get_connection_login_id();

	auto current_gameInfo = SoftwareManager::get()->get_connection_game(name_game);
	uint32_t connection_game_id = current_gameInfo.first;
	if (!current_gameInfo.second) {
		connection_game_id = WebManager::get()->start_game(software_id, user_email, user_token, login_id, name_game);

		std::shared_ptr<GameInfo> gameInfo = std::make_shared<GameInfo>();
		gameInfo->process_name = name_game;
		gameInfo->timer.reset();

		SoftwareManager::get()->add_connections_list(connection_game_id, gameInfo);
	}
	else
		this->update_game(connection_game_id);	
	
	return connection_game_id;
}
bool Comunication::update_game(uint32_t connection_id){
	std::string user_email = InfoManager::get()->get_current_user_email();
	
	std::pair<void*, uint32_t> user_token_pair = InfoManager::get()->get_current_user_token();
	if (!user_token_pair.first)
		return false;

	std::string* user_token_ptr = (static_cast<std::string*>(user_token_pair.first));
	std::string user_token = std::string(user_token_ptr->begin(), user_token_ptr->end());

	uint32_t software_id = InfoManager::get()->get_current_software_id();
	return WebManager::get()->update_game(software_id, user_email, user_token, connection_id);
}

bool Comunication::check_mismatch_machine_id(){
	std::pair<void*, uint32_t> machine_id_pair = InfoManager::get()->get_current_machine_id();
	if (!machine_id_pair.first)
		return true;

	std::string* machine_id = (static_cast<std::string*>(machine_id_pair.first));
	std::string user_email = InfoManager::get()->get_current_user_email();

	bool response = WebManager::get()->check_mismatch_machine_id(std::string(machine_id->begin(), machine_id->end()), user_email);
	if (!response)
		return false;

	this->execute_machine_callback();
	return true;
}

extern "C" {
	int DLLDIR run() {
		return Comunication::get()->run();
	}

	bool DLLDIR check_mismatch_machine_id(){
		return Comunication::get()->check_mismatch_machine_id();
	}

	bool DLLDIR update_game(uint32_t connection_id) {
		return Comunication::get()->update_game(connection_id);
	}
	int DLLDIR start_game(wchar_t* game_name) {
		return Comunication::get()->start_game(game_name);
	}
	
	bool DLLDIR update_login(uint32_t connection_id) {
		return Comunication::get()->update_login(connection_id);
	}
	int DLLDIR start_login() {
		return Comunication::get()->start_login();
	}

	int DLLDIR set_software_version(uint32_t software_version){
		return Comunication::get()->set_software_version(software_version);
	}
	int DLLDIR set_software_id(uint32_t software_id){
		return Comunication::get()->set_software_id(software_id);
	}

	int DLLDIR set_user_email(wchar_t* user_email){
		return Comunication::get()->set_user_email(user_email);
	}
	int DLLDIR set_user_token(void* data, uint32_t len){
		return Comunication::get()->set_user_token(data, len);
	}
	int DLLDIR set_machine_id(void* data, uint32_t len){
		return Comunication::get()->set_machine_id(data, len);
	}
	int DLLDIR set_user_trial(bool trial) {
		return Comunication::get()->set_user_trial(trial);
	}

	int DLLDIR set_message_times_in_times_callback(params_t::message_callback _callback){
		return Comunication::get()->set_message_times_in_times_callback(_callback);
	}
	int DLLDIR set_message_callback(params_t::message_callback _callback){
		return Comunication::get()->set_message_callback(_callback);
	}
	int DLLDIR set_machine_callback(params_t::machine_mismatch_callback _callback){
		return Comunication::get()->set_machine_mismatch_callback(_callback);
	}
	int DLLDIR set_update_game_callback(params_t::update_game_callback _callback) {
		return Comunication::get()->set_update_game_callback(_callback);
	}
}